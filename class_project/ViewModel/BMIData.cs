﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace class_project.ViewModel
{
    public class BMIData
    {
        [Display(Name = "體重")]
        [Required(ErrorMessage = "這裡是必填欄位/輸入錯誤。")]
        [Range(30, 150, ErrorMessage = "請輸入30-150的數值")]
        public float weight { get; set; }

        [Display(Name = "身高")]
        [Required(ErrorMessage = "這裡是必填欄位/輸入錯誤。")]
        [RangeAttribute(50, 250, ErrorMessage = "請輸入30-150的數值"))]
        public float height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}