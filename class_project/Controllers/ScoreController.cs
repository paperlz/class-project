﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace class_project.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(int score)
        {
            string level = "";
            if (score <= 19)
            {
                level = "E";
            }
            else if (score >= 19 && score <= 39)
            {
                level = "D";
            }
            else if (score >= 40 && score <= 59)
            {
                level = "C";
            }
            else if (score >= 60 && score <= 79)
            {
                level = "B";
            }
            else if (score >= 80 && score <= 100)
            {
                level = "A";
            }
            ViewBag.level = level;
            ViewBag.score = score;
            return View();
        }
    }

}