﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace class_project.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string[] fruits = { "蘋果", "香蕉", "荔枝", "鳳梨", "西瓜" };

            ViewBag.Data = fruits;
            ViewData["Data"] = fruits;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "你好哦!.";

            return View();
        }
    }
}